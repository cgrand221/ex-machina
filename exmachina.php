<?php

/*
Credit: "Ex Machina" film team
See: https://i.imgur.com/C44iJeR.jpg
-------------------------------
*/


function  sieve($n){
	//Compute primes using sieve of Eratosthemes
	$x=[];
	for($i=1;$i<$n;$i++){
		$x[$i] = 1;
	}
	$x[1] = 0;
	for($i=2;$i<$n/2;$i++){
		$j = 2 * $i;
		while ($j < $n){
			$x[$j]=0;
			$j = $j+$i;
		}
	}
	return $x;
}

function prime($n,$x){
	// Find nth prime
	$i = 1;
	$j = 1;
	while($j <= $n){
		if($x[$i] == 1){
			$j = $j + 1;
		}
		$i = $i + 1;
	}
	return $i-1;
}

// compute Bluebook unlock code

$x=sieve(10000);

$code = [1206,301,384,5];
$key =[1,1,2,2,];

foreach([73,83,66,78,32,61,32] as $i){
	echo chr($i);
}
for ($i=0;$i<4;$i++){
	echo (prime($code[$i], $x)-$key[$i]);
}
echo "\n";


